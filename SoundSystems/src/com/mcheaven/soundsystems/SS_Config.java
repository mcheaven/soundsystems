package com.mcheaven.soundsystems;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.Configuration;

public class SS_Config {
	
	SS_Main plugin;

	public SS_Config(SS_Main plugin) {
		this.plugin = plugin;
	}
	
	private String version;
	private Set<String> songs;
	
	public void createConfig() {
		Logger logger = plugin.getLogger();
		Configuration config = plugin.getConfig();
		version = plugin.getDescription().getVersion();
		String configversion = config.getString("ConfigVersion", "bla");
		if (!configversion.equals(version)) {
			config.set("ConfigVersion", version);
			if(!config.contains("Songs")) {
				config.set("Songs.song1", "&note_piano:20:0:0:20&note_piano:20");
			}
			plugin.saveConfig();
			logger.log(Level.INFO, "Config Updated to Version " + version);
		}
		loadValues();
		logger.log(Level.INFO, "Config loaded");
	}
	
	private void loadValues() {
		Configuration config = plugin.getConfig();
		version = config.getString("ConfigVersion");
		songs = config.getConfigurationSection("Songs").getKeys(true);
		for(String songName : songs) {
			new SS_Song(plugin, songName).init(config.getString("Songs."+songName));
		}
	}
	

}
