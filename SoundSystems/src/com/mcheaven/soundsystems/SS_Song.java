package com.mcheaven.soundsystems;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Location;

public class SS_Song {

	private SS_Main plugin;
	
	private String songName;
	private Location loc;
	
	public SS_Song(SS_Main plugin, String songName) {
		this.plugin = plugin;
		this.songName = songName;
	}
	
	private HashSet<SS_Sound> sounds = new HashSet<SS_Sound>();
	
	public void init(String song) {
		List<Integer> soundPos = new ArrayList<Integer>();
		int i = 0;
		for(char songChar; i < song.length(); i++) {
			songChar = song.charAt(i);
			if(songChar == '&')
				soundPos.add(i);
		}
		String singleSong;
		for(int startPos : soundPos) {
			singleSong = song.substring(startPos);
			singleSong = singleSong.substring(0, SS_Util.nextBorder(singleSong, '&'));
			if(SS_Sound.valueOf(singleSong, plugin) != null) {
				sounds.add(SS_Sound.valueOf(singleSong, plugin));
			}//WIP
		}
		plugin.songs.put(songName, this);
	}
	
	public void setLocation(Location newloc) {
		this.loc = newloc;
		for(SS_Sound sound : sounds)
			sound.setLocation(loc);
	}
	
	public void playSong() {
		for(SS_Sound sound : sounds)
			sound.startSound();
	}

}
