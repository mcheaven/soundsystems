package com.mcheaven.soundsystems;

import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import com.mcheaven.soundsystems.commands.CommandHandler;

public class SS_Main extends JavaPlugin {

	private String path = getDataFolder().getAbsolutePath()
			+ "/playingSounds.bin";
	public HashMap<Integer, SS_Sound> playingSounds = new HashMap<Integer, SS_Sound>();
	public HashMap<String, SS_Song> songs = new HashMap<String, SS_Song>();
	private HashMap<Integer, SS_Sound> loadingSounds = new HashMap<Integer, SS_Sound>();
	public final String premsg = ChatColor.GOLD + "[SoundSystems] "
			+ ChatColor.RED;

	private CommandHandler ch;
	private SS_Config config;

	public void onEnable() {

		ch = new CommandHandler(this);
		config = new SS_Config(this);
		getCommand("soundsystems").setExecutor(ch);
		config.createConfig();
		loadPlayingSounds();

	}

	public void onDisable() {
		SS_Util.saveSound(playingSounds, path);
	}

	public void addPlayingSound(Integer taskId, SS_Sound sound) {
		playingSounds.put(taskId, sound);
	}

	public void removePlayingSound(Integer taskId) {
		if (playingSounds.containsKey(taskId))
			playingSounds.remove(taskId);
	}

	public String getPath() {
		return path;
	}

	private void loadPlayingSounds() {
		if (SS_Util.loadSound(path) == null)
			return;
		loadingSounds = SS_Util.loadSound(path);
		if (loadingSounds.isEmpty())
			return;
		for (Integer taskId : loadingSounds.keySet()) {
			SS_Sound sOld = loadingSounds.get(taskId);
			Location loc = getServer().getWorld(sOld.getWorldName())
					.getBlockAt(sOld.getX(), sOld.getY(), sOld.getZ())
					.getLocation();
			SS_Sound sNew = new SS_Sound(this, loc, sOld.getSound(),
					sOld.getVolume(), sOld.getPitch(), sOld.getTimer(), 0);
			sNew.startSound();
			loadingSounds.remove(taskId);
		}
	}
}
