package com.mcheaven.soundsystems;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SS_Util {
	
	public static <O extends Object> void saveSound(O object, String path) {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(path));
			oos.writeObject(object);
			oos.flush();
			oos.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <O extends Object> O loadSound(String path) {
		O result = null;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
			result = (O)ois.readObject();
			ois.close();
		} catch(Exception e) {
			//Warnings Missing..
		}
		return result;
	}
	
	public static int nextBorder(String checkString, char compareChar) {
		int border = 0;
		for(; border < checkString.length(); border++) {
			if(checkString.charAt(border) == compareChar && border != 0) {
				break;
			}
		}
		if(border == 0)
			border = checkString.length();
		return border;
	}
	
	public static boolean isNumber(String checkString) {
		boolean number = false;
		try {
			if(Integer.valueOf(checkString) != null)
				number = true;
		} catch(NumberFormatException e) {
			number = false;
		}
		return number;
	}

}
