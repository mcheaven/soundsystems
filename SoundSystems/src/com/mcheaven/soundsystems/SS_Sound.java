package com.mcheaven.soundsystems;

import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class SS_Sound implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8352839062645090096L;

	private final transient SS_Main plugin;

	private transient Location loc;
	
	private int x;
	private int y;
	private int z;
	private String worldName;
	private Sound sound;
	private float volume;
	private float pitch;
	private long timer;
	private long offset;
	private transient int taskId = 0;
	private transient World world;
	
	
	public SS_Sound(SS_Main instance, Location loc, Sound sound, float volume, float pitch, long timer, long offset) {
		plugin = instance;
		this.loc = loc;
		this.sound = sound;
		this.volume = volume;
		this.pitch = pitch;
		this.timer = timer;
		this.offset = offset;
		if(loc == null)
			return;
		this.world = loc.getWorld();
		this.x = loc.getBlockX();
		this.y = loc.getBlockY();
		this.z = loc.getBlockZ();
		this.worldName = world.getName();
	}
	
	public void startSound() {
		if(timer != 0) {
			startTimedSound(loc);
			return;
		} else if(offset != 0) {
			startDelayedSound();
		} else
			world.playSound(loc, sound, volume, pitch);
	}
	
	private void startDelayedSound() {
        BukkitTask task = new BukkitRunnable() {
       	 
            @Override
            public void run() {
            	world.playSound(loc, sound, volume, pitch);
            }
 
        }.runTaskLater(plugin, offset);

        taskId = task.getTaskId();
        plugin.addPlayingSound(taskId, this);
	}

	private void startTimedSound(final Location loc) {
        BukkitTask task = new BukkitRunnable() {
        	 
            @Override
            public void run() {
            	world.playSound(loc, sound, volume, pitch);
            }
 
        }.runTaskTimer(plugin, offset, timer);
        taskId = task.getTaskId();
        plugin.addPlayingSound(taskId, this);
	}
	
	public boolean stopTimedSound() {
		if(taskId != 0) {
			Bukkit.getScheduler().cancelTask(taskId);
			plugin.removePlayingSound(taskId);
			return true;
		} else
			return false;
	}
	
	public static SS_Sound valueOf(String stringSound, SS_Main plugin) {
		long configTimer = 0L;
		long configOffset = 0L;
		int configVolume;
		int configPitch = 0;
		Sound configSound;
		if(!stringSound.contains("&"))
			return null;
		int effectEndPos = SS_Util.nextBorder(stringSound, ':');
		String effectName = stringSound.substring(1, effectEndPos).toUpperCase();
		stringSound = stringSound.substring(effectEndPos+1);
		if(Sound.valueOf(effectName) != null)
			configSound = Sound.valueOf(effectName);
		else
			return null;
		if(SS_Util.isNumber(stringSound)) {
			configVolume = Integer.valueOf(stringSound);
			return new SS_Sound(plugin, null, configSound, configVolume, configPitch, configTimer, configOffset);
		} else {
			String stringVolume = stringSound.substring(0, SS_Util.nextBorder(stringSound, ':'));
			stringSound = stringSound.substring(SS_Util.nextBorder(stringSound, ':')+1);
			configVolume = Integer.valueOf(stringVolume);
		}
		if(SS_Util.isNumber(stringSound)) {
			configPitch = Integer.valueOf(stringSound);
			return new SS_Sound(plugin, null, configSound, configVolume, configPitch, configTimer, configOffset);
		} else {
			String stringPitch = stringSound.substring(0, SS_Util.nextBorder(stringSound, ':'));
			stringSound = stringSound.substring(SS_Util.nextBorder(stringSound, ':')+1);
			configPitch = Integer.valueOf(stringPitch);
		}
		if(SS_Util.isNumber(stringSound)) {
			configTimer = Long.valueOf(stringSound);
			return new SS_Sound(plugin, null, configSound, configVolume, configPitch, configTimer, configOffset);
		} else {
			String stringTimer = stringSound.substring(0, SS_Util.nextBorder(stringSound, ':'));
			stringSound = stringSound.substring(SS_Util.nextBorder(stringSound, ':')+1);
			configTimer = Long.valueOf(stringTimer);
		}
		if(SS_Util.isNumber(stringSound)) {
			configOffset = Long.valueOf(stringSound);
			return new SS_Sound(plugin, null, configSound, configVolume, configPitch, configTimer, configOffset);
		}
		
		
		return new SS_Sound(plugin, null, configSound, configVolume, configPitch, configTimer, configOffset);
	}
	

	//Getter and Setters:

	public Sound getSound() {
		return sound;
	}

	public void setSound(Sound sound) {
		this.sound = sound;
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public long getTimer() {
		return timer;
	}

	public void setTimer(long timer) {
		this.timer = timer;
	}

	public void setLocation(Location loc) {
		this.loc = loc;
		this.x = loc.getBlockX();
		this.y = loc.getBlockY();
		this.z = loc.getBlockZ();
		this.world = loc.getWorld();
		this.worldName = world.getName();
	}
	
	public Location getLocation() {
		return loc;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public String getWorldName() {
		return worldName;
	}

}
