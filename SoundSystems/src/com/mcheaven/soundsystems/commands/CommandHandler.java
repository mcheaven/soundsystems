package com.mcheaven.soundsystems.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import com.mcheaven.soundsystems.SS_Main;


public class CommandHandler implements CommandExecutor {
	
	
	private final SS_Main plugin;
	
	public CommandHandler(SS_Main instance) {
		plugin = instance;
	}
	
	
	
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
			if (args.length < 1) {
				sender.sendMessage(plugin.premsg +"Version "+ plugin.getDescription().getVersion());
				return true;
			}
			//args[0] is the parameter after the command name for example: "/soundsystems play"
			if (args[0].equalsIgnoreCase("play")) {
				return new PlayCommand(plugin).execute(sender, args, plugin.premsg);
			}
			if (args[0].equalsIgnoreCase("cancel")) {
				return new CancelCommand(plugin).execute(sender, args, plugin.premsg);
			}
			if (args[0].equalsIgnoreCase("help")) {
				return new HelpCommand().execute(sender, args, plugin.premsg);
			}
		return true;
	}
	

}
