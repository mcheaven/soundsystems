package com.mcheaven.soundsystems.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitScheduler;

import com.mcheaven.soundsystems.SS_Main;

public class CancelCommand {

	private final SS_Main plugin;
	private String premsg;
	private BukkitScheduler scheduler;
	
	public CancelCommand(SS_Main instance) {
		plugin = instance;
	}
	
	public boolean execute(CommandSender sender, String[] args, String premsg) {
		this.premsg = premsg;
		scheduler = Bukkit.getServer().getScheduler();
		if(args.length == 1) {
			playingSoundOverview(sender);
			return true;
		}
		if(args[1].equalsIgnoreCase("all")) {
			cancelAll(sender);
			return true;
		} else {
			cancelSound(sender, args[1]);
		}
		return true;
	}
	
	private void cancelAll(CommandSender sender) {
		scheduler.cancelTasks(plugin);
		sender.sendMessage(premsg+"Cleared every playing sound!");
		plugin.playingSounds.clear();
	}
	
	private void playingSoundOverview(CommandSender sender) {
		sender.sendMessage(premsg+"Playing sounds "+ChatColor.GRAY+ChatColor.ITALIC+"["+ChatColor.GOLD+ChatColor.ITALIC+"ID "+ChatColor.RED+ChatColor.ITALIC+"Soundeffect"+ChatColor.GRAY+ChatColor.ITALIC+"]"+ChatColor.RESET+ChatColor.GOLD+":");
		for(Integer key : plugin.playingSounds.keySet()) {
			sender.sendMessage(ChatColor.GOLD+key.toString()+" "+ChatColor.RED+plugin.playingSounds.get(key).getSound());
		}
	}
	
	private void cancelSound(CommandSender sender, String sound) {
		Integer soundKey = null;
		try {
			soundKey = Integer.valueOf(sound);
		} catch(NumberFormatException e) {
			//Soundkey is not a Number
		}
		if(soundKey != null) {
			plugin.playingSounds.get(soundKey).stopTimedSound();
		} else {
			//Maybe he wants to remove every playing Sound using a specific "Instrument"
			for(Integer soundId : plugin.playingSounds.keySet()) {
				if(plugin.playingSounds.containsKey(soundId)) {
					//todo Yoro bruder
				}
			}
		}
	}

}
