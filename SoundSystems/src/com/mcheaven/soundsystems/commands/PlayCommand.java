package com.mcheaven.soundsystems.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.mcheaven.soundsystems.SS_Main;
import com.mcheaven.soundsystems.SS_Sound;

public class PlayCommand {
	
	private final SS_Main plugin;
	
	private String premsg;
	private SS_Sound ss_sound;
	private float volume = 20F;
	private float pitch = 0F;
	private long timer = 0;
	private Sound sound;
	private Location loc;

	public PlayCommand(SS_Main instance) {
		plugin = instance;
	}
	
	
	//Main Method in this Class
	public boolean execute(CommandSender sender, String[] args, String premsg) {
		this.premsg = premsg;
		volume = 20F;
		pitch = 0F;
		if (args.length < 2) { //in this case the player didnt write enough parameters
			sender.sendMessage(premsg+"Not enough arguments");
			return false;
		}
		ss_sound = new SS_Sound(plugin, null, sound, volume, pitch, 0, 0);
		if(args.length < 3) {
			if (!(sender instanceof Player)) { //Checks if the Command was executed by a player
				sender.sendMessage(premsg+"Not usable from console");
				return true;
			}
		}
		if(addSong(args, sender)) {
			if(!addLocation(args, sender))
				return false;
			plugin.songs.get(args[1]).setLocation(loc);
			plugin.songs.get(args[1]).playSong();
			return true;
		}
		addSound(args, sender);
		if(!addLocation(args, sender))
			return false;
		if(!addVol(args, sender))
			return false;
		if(!addPit(args, sender))
				return false;
		if(!addTimer(args, sender))
			return false;
		ss_sound.startSound();
		return true;
	}

	//This Method checks if the Volume is a correct Number sets it in the ss_sound object
	private boolean addVol(String[] args, CommandSender sender) {
		if (args.length > 3) {
			try {
				volume = Float.valueOf(args[3]);
			} catch (NumberFormatException e1) {
				sender.sendMessage(premsg+ChatColor.BOLD+args[3]+ChatColor.RESET+ChatColor.RED+
						" has to be a number!");
				return false;
			}
			ss_sound.setVolume(volume);
		}
		return true;
	}
	
	//This Method checks if the Pitch is a correct number and sets it in the ss_sound object
	private boolean addPit(String[] args, CommandSender sender) {
		if (args.length > 4) {
			try {
				pitch = Float.valueOf(args[4]);
			} catch (NumberFormatException e1) {
				sender.sendMessage(premsg+ChatColor.BOLD+args[4]+ChatColor.RESET+ChatColor.RED+
						" has to be a number!");
				return false;
			}
			ss_sound.setPitch(pitch);
		}
		return true;
	}
	
	private boolean addLocation(String[] args, CommandSender sender) {
		Player tplayer;
		if(args.length < 3)
			tplayer = (Player) sender;
		else
			tplayer = plugin.getServer().getPlayer(args[2]); //gets target player
		if (tplayer == null) {
			sender.sendMessage(premsg+"Player "+ChatColor.BOLD+args[2]+ChatColor.RESET+ChatColor.RED+
					" is not online!");
			return false;
		}
		loc = tplayer.getEyeLocation();
		ss_sound.setLocation(loc);
		return true;
	}
	
	private boolean addSound(String[] args, CommandSender sender) {
		try {
			sound = Sound.valueOf(args[1].toUpperCase());
		} catch(Exception e) {
			sender.sendMessage(premsg+ChatColor.BOLD+args[1]+ChatColor.RESET+ChatColor.RED+
					" is not a correct Soundeffect!");
			return false;
		}
		ss_sound.setSound(sound);
		return true;
	}
	
	private boolean addSong(String[] args, CommandSender sender) {
		String song = args[1];
		if(plugin.songs.containsKey(song))
			return true;
		return false;
	}

	private boolean addTimer(String[] args, CommandSender sender) {
		if (args.length > 5) {
			try {
				timer = Long.valueOf(args[5]);
			} catch (NumberFormatException e1) {
				sender.sendMessage(premsg+ChatColor.BOLD+args[5]+ChatColor.RESET+ChatColor.RED+
						" has to be a number!");
				return false;
			}
			ss_sound.setTimer(timer);
		}
		return true;
	}
}
