package com.mcheaven.soundsystems.commands;

import org.bukkit.command.CommandSender;

public class HelpCommand {
	
	private String premsg;
	
	public boolean execute(CommandSender sender, String[] args, String pPremsg) {
		this.premsg = pPremsg;
		
		if (args.length < 2){
			sender.sendMessage(premsg+"/soundsystems play [soundeffect] (player) (volume) (pitch) (timer)");
			sender.sendMessage(premsg+"/soundsystems cancel (all/id)");
			return true;
		}
		
		if (args[1].equalsIgnoreCase("play")) {
			sender.sendMessage(premsg+"[soundeffect] Name of the soundeffect");
			sender.sendMessage(premsg+"(player) Plays the sound on players position ");
			sender.sendMessage(premsg+"(volume) How far you can hear the sound in blocks");
			sender.sendMessage(premsg+"(pitch) Let's you pitch the sound");
			sender.sendMessage(premsg+"(timer) Time between each sound");
		}
		
		if (args[1].equalsIgnoreCase("cancel")) {
			sender.sendMessage(premsg+"Shows sounds which are currently playing");
			sender.sendMessage(premsg+"(all) Stops all sounds");
			sender.sendMessage(premsg+"(id) Stops specific sound");
		}
		return true;
	}

}
